# Hands on genetic algorithms

Taken from https://github.com/CodingTrain/website/tree/main/more/archive/nature-of-code/9-genetic-algorithms/9.4-looking-at-code/P5 check his courses for more details! https://thecodingtrain.com/more/archive/nature-of-code/9-genetic-algorithms/9.4-looking-at-code.html 

## Getting started

Simply open the html files inside any folder. I recommend LiveShare with VSCode so it updates automatically but just plainly openening the html also works

## Ok what do I do?
You have three tasks:

### Shakespeare monkeys
Go to the folder 9.3-shakespeare-monkey-example. If you open index.html you'll see a bunch of random phrases appearing. Your job is to complete the GA algorithm to fix this and make the random phrases lean towards "To be or not to be"

For this you'll find in DNA.js two functions that are not done yet but docummented: calcFitness and crossover. Read the comments they have and complete them. **This is your first task**

Next you'll want to experiment with the popmax and mutation parameters in sketch.js in order to answer the following questions. **This is your second task**

1. What happens when population is low (1-15)
1. What happens when mutation is high ( > 35) ?
1. What happens when the population is high ( > 1000 )? And when it’s VERY high?


### Rocket ships
Go to the folder 9.5-fitness-genotype-vs-phenotype. If you open index.html you'll see several rockets flying randomly. Your job is to complete the GA algorithm for the rockets to train properly and reach their target.

For this you'll find in rocket.js the fitness function undone but commented. Read the comments and do your best to complete it.

Keep experimenting with the fitness function until one of the rockets hits the target. You can also keep playing around to optimize the function in terms of speed. **This is your third task**

### Knapsack 
Knapsack problem

Go to the folder "knapsack problem" you'll notice it's empty. In this exercice you will solve the knackbag problem all from scratch on your own using a genetic algorithm.

If you're unfamiliar with the knapsack problem you can refer to the description here https://www.geeksforgeeks.org/0-1-knapsack-problem-dp-10/


For this problem you have the following values for weights and values:

    weight: 10, 20, 30, 1

    value: 60, 1000, 120, 1

    weight_limit: 52

The objective is to find what items and what amount of each type will maximize the value without going over the weight limit.


You can try to make the whole genetic algorithm system yourself from scratch or use an existing library. For example, **geneticalgorithm** for python. I'll leave instructions below on how to install it:

1. Install python if you don't have it ```sudo apt install python3.8```
1. Install pip if you don't have it ```sudo apt install python3-pip```
1. Install the required libraries with pip:
1. ```pip install numpy ```
1. ```pip install matplotlib ```
1. ```pip install geneticalgorithm```

And you're set to go. Refer to https://pypi.org/project/geneticalgorithm/ for documentation on how to use this library.
