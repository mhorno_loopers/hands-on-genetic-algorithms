// Shakespeare
// The Nature of Code
// The Coding Train / Daniel Shiffman
// https://youtu.be/nrKjSeoc7fc
// https://thecodingtrain.com/more/achive/nature-of-code/9-genetic-algorithms/9.3-shakespeare-monkey-example.html
// https://editor.p5js.org/codingtrain/sketches/GUZKUFxKo

// http://natureofcode.com

// Genetic Algorithm, Evolving Shakespeare

// A class to describe a pseudo-DNA, i.e. genotype
//   Here, a virtual organism's DNA is an array of character.
//   Functionality:
//      -- convert DNA into a string
//      -- calculate DNA's "fitness"
//      -- mate DNA with another set of DNA
//      -- mutate DNA

function newChar() {
  let c = floor(random(63, 122));
  if (c === 63) c = 32;
  if (c === 64) c = 46;

  return String.fromCharCode(c);
}

// Constructor (makes a random DNA)
class DNA {
  constructor(num) {
    // The genetic sequence
    this.genes = [];
    this.fitness = 0;
    for (let i = 0; i < num; i++) {
      this.genes[i] = newChar(); // Pick from range of chars
    }
  }

  // Converts character array to a String
  getPhrase() {
    return this.genes.join("");
  }

  /**
   * Fitness function. Sets fitness high based on how similar to target the phenotype is
   * @param {string} target What the subject should aim to be.
   */

  calcFitness(target) {
    // TODO: Compare genes with target in a way in which the closer genes are to target the closer the return value is to 1
    // Example: If target is 'secret' then:
    //        If genes are 'secret' then fitness is 1
    //        If genes are 'acret' then fitness is not 1
    //        If genes are 'fasdf' then fitness is not 1 but lower than 'acret'

    this.fitness = 0.5 // This is wrong. Change me!!

    // Make the function exponential. If your fitness is normalized don't touch this (faster learning)
    this.fitness = pow(this.fitness, 4);
  }

  /**
   * Crossover function
   * @param {DNA} partner What the subject should aim to be.
   * @returns {DNA} child DNA with mixed genes from parents
   */
  crossover(partner) {
    // Create a DNA child and make its genes be a mix of its parents( one parent is target and the other one the object itself)

    // Tip: Each DNA has the same number of genes. Pick a point at random in between its length and make the child have 
    //      the child have dna from one parent from 0 to that point and the rest from the other parent.
    
    return new DNA(this.genes.length); // Delete me !
  }



  // Based on a mutation probability, picks a new random character
  mutate(mutationRate) {
    for (let i = 0; i < this.genes.length; i++) {
      if (random(1) < mutationRate) {
        this.genes[i] = newChar();
      }
    }
  }
}
